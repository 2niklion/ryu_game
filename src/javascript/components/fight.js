import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    const isCriticalAvailable = {
      firstFighter: true,
      secondFighter: true
    };

    const firstFighterHealthBar = document.querySelector('#left-fighter-indicator');
    const secondFighterHealthBar = document.querySelector('#right-fighter-indicator');

    const pressed = new Set();

    const keydownListener = (e) => {
      pressed.add(e.code);

      if (pressed.has(controls.PlayerOneCriticalHitCombination[0])
        && pressed.has(controls.PlayerOneCriticalHitCombination[1])
        && pressed.has(controls.PlayerOneCriticalHitCombination[2])
        && isCriticalAvailable.firstFighter
      ) {
        isCriticalAvailable.firstFighter = false;
        secondFighterHealth -= firstFighter.attack * 2;

        const healthPercentage = 100 * secondFighterHealth / secondFighter.health;
        secondFighterHealthBar.setAttribute('style', `width: ${healthPercentage}%`);


        setTimeout(() => {
          isCriticalAvailable.firstFighter = true;
        }, 10000);
      }

      if (pressed.has(controls.PlayerTwoCriticalHitCombination[0])
        && pressed.has(controls.PlayerTwoCriticalHitCombination[1])
        && pressed.has(controls.PlayerTwoCriticalHitCombination[2])
        && isCriticalAvailable.secondFighter
      ) {
        isCriticalAvailable.secondFighter = false;
        firstFighterHealth -= secondFighter.attack * 2;

        const healthPercentage = 100 * firstFighterHealth / firstFighter.health;
        firstFighterHealthBar.setAttribute('style', `width: ${healthPercentage}%`);


        setTimeout(() => {
          isCriticalAvailable.secondFighter = true;
        }, 10000);
      }

      if (pressed.has(controls.PlayerOneAttack) && !pressed.has(controls.PlayerOneBlock)) {
        if (!pressed.has(controls.PlayerTwoBlock)) {
          secondFighterHealth -= getDamage(firstFighter, secondFighter);

          const healthPercentage = 100 * secondFighterHealth / secondFighter.health;
          secondFighterHealthBar.setAttribute('style', `width: ${healthPercentage}%`);
        }
      }

      if (pressed.has(controls.PlayerTwoAttack) && !pressed.has(controls.PlayerTwoBlock)) {
        if (!pressed.has(controls.PlayerOneBlock)) {
          firstFighterHealth -= getDamage(secondFighter, firstFighter);

          const healthPercentage = 100 * firstFighterHealth / firstFighter.health;
          firstFighterHealthBar.setAttribute('style', `width: ${healthPercentage}%`);
        }
      }

      if (firstFighterHealth <= 0 || secondFighterHealth <= 0) {
        const winner = firstFighterHealth > 0 ? firstFighter : secondFighter;
        document.removeEventListener('keydown', keydownListener);
        document.removeEventListener('keyup', keyupListener);
        resolve(winner);
      }
    };

    const keyupListener = (e) => pressed.delete(e.code);

    document.addEventListener('keydown', keydownListener);
    document.addEventListener('keyup', keyupListener);
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
  // return damage
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const criticalHitChance = Math.random() * (2 - 1) + 1;

  return attack * criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const dodgeChance = Math.random() * (2 - 1) + 1;

  return defense * dodgeChance;
  // return block power
}